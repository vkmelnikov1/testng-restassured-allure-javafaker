package tests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import models.Company;
import models.CompanyRequest;
import models.ErrorResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.BaseTest;
import utils.RestUtils;
import utils.TestInfoContext;

import java.lang.reflect.Method;

import static org.hamcrest.Matchers.equalTo;

public class CreateCompanyTest extends BaseTest {

    RequestSpecification request;
    RestUtils restUtils;

    @BeforeMethod(alwaysRun = true)
    void setUp(Method method) {
        TestInfoContext.addTest(method.getAnnotation(Step.class).value());
        request = (restUtils = new RestUtils()).given()
                .log().all()
                .contentType(ContentType.JSON)
                .baseUri("http://users.bugred.ru/tasks/rest/createcompany");
    }

    @Test
    @Step("Регистрация комании")
    void registerCompany() {
        JsonNode receivedCompany;
        CompanyRequest requestedCompany = new CompanyRequest(faker.company().name(), "OOO", new String[]{faker.internet().emailAddress(), faker.internet().emailAddress()}, "milli@mail.ru");
        try {
            receivedCompany = request.when()
                    .body(requestedCompany)
                    .post()
                    .then()
                    .log().all()
                    .assertThat()
                    .statusCode(201)
                    .body("type", equalTo("success"))
                    .body("company.name", equalTo(requestedCompany.getCompany_name()))
                    .body("company.type", equalTo(requestedCompany.getCompany_type()))
                    .body("company.users", equalTo(requestedCompany.getCompany_users()))
                    .extract()
                    .as(JsonNode.class);
        } finally {
            restUtils.close();
        }
        Company convertedCompany = new ObjectMapper().convertValue(receivedCompany, Company.class);
    }

    @Test
    @Step("Регистрация комании с пустым полем имя")
    void registerCompanyWithEmptyName() {
        registerWithMalformedInput(new CompanyRequest("", "OOO", new String[]{faker.internet().emailAddress(), faker.internet().emailAddress()}, "milli@mail.ru"), "Введите название компании");
    }

    @Test
    @Step("Регистрация комании с пустым полем типа компании")
    void registerCompanyWithEmptyCompanyType() {
        registerWithMalformedInput(new CompanyRequest(faker.company().name(), "", new String[]{faker.internet().emailAddress(), faker.internet().emailAddress()}, "milli@mail.ru"), "Введите тип компании");
    }

    @Test
    @Step("Регистрация комании c пустым полем сотрудников")
    void registerCompanyWithEmptyCompanyUsers() {
        registerWithMalformedInput(new CompanyRequest(faker.company().name(), "OOO", new String[]{}, "milli@mail.ru"), "Введите электронные почты сотрудников компании");
    }

    @Test
    @Step("Регистрация комании без владельца")
    void registerCompanyWithEmptyOwner() {
        registerWithMalformedInput(new CompanyRequest(faker.company().name(), "OOO", new String[]{faker.internet().emailAddress(), faker.internet().emailAddress()}, ""), "Введите емейл владельца");
    }


    private void registerWithMalformedInput(CompanyRequest requestedCompany, String errorMessage) {
        JsonNode response;
        try {
            response = request.when()
                    .body(requestedCompany)
                    .post()
                    .then()
                    .log().all()
                    .assertThat()
                    .statusCode(400)
                    .extract()
                    .as(JsonNode.class);
        } finally {
            restUtils.close();
        }
        ErrorResponse errorResponse = new ObjectMapper().convertValue(response, ErrorResponse.class);
        Assert.assertEquals(errorResponse, new ErrorResponse("error", errorMessage));
    }
}
