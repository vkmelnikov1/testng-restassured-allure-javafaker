package tests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.SneakyThrows;
import models.ErrorResponse;
import models.Worker;
import models.WorkerResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.BaseTest;
import utils.RestUtils;
import utils.TestInfoContext;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static utils.TimeUtils.getTimeInSeconds;

public class CreateUserTest extends BaseTest {

    RequestSpecification request;
    RestUtils restUtils;

    @BeforeMethod(alwaysRun = true)
    void setUp(Method method) {
        TestInfoContext.addTest(method.getAnnotation(Step.class).value());
        request = (restUtils = new RestUtils()).given()
                .log().all()
                .contentType(ContentType.JSON)
                .baseUri("http://users.bugred.ru/tasks/rest/createuser");
    }

    @SneakyThrows
    @Test
    @Step("Регистрация работника")
    void createUser() {
        JsonNode receivedWorker;
        String workerStr = readFile("templates/worker.json");
        Worker requestedWorker = createWorker(workerStr);
        try {
            receivedWorker = request.when()
                    .body(requestedWorker)
                    .post()
                    .then()
                    .log().all()
                    .assertThat()
                    .statusCode(201)
                    .body("email", equalTo(requestedWorker.getEmail()))
                    .body("name", equalTo(requestedWorker.getName()))
                    .body("name1", equalTo(requestedWorker.getName1()))
                    .body("hobby", equalTo(requestedWorker.getHobby()))
                    .body("surname1", equalTo(requestedWorker.getSurname1()))
                    .body("fathername1", equalTo(requestedWorker.getFathername1()))
                    .body("dog", equalTo(requestedWorker.getDog()))
                    .body("parrot", equalTo(requestedWorker.getParrot()))
                    .body("cavy", equalTo(requestedWorker.getCavy()))
                    .body("hamster", equalTo(requestedWorker.getHamster()))
                    .body("squirrel", equalTo(requestedWorker.getSquirrel()))
                    .body("phone", equalTo(requestedWorker.getPhone()))
                    .body("adres", equalTo(requestedWorker.getAdres()))
                    .body("gender", equalTo(requestedWorker.getGender()))
                    .body("date_start.sec", equalTo(getTimeInSeconds(requestedWorker.getDate_start())))
                    .body("birthday.sec", equalTo(getTimeInSeconds(requestedWorker.getBirthday())))
                    .body("role", hasItem("user"))
                    .body("tasks[0].id", equalTo(new WorkerResponse.Task(requestedWorker.getTasks().get(0)).getId()))
                    .body("companies", hasSize(2))
                    .body("companies.id", hasItem(requestedWorker.getCompanies().get(0)))
                    .body("companies.id", hasItem(requestedWorker.getCompanies().get(1)))
                    .extract()
                    .as(JsonNode.class);
        } finally {
            restUtils.close();
        }
        WorkerResponse convertedWorker = new ObjectMapper().convertValue(receivedWorker, WorkerResponse.class);
    }

    @SneakyThrows
    @Test
    @Step("Регистрация работника c минимумом полей")
    void createUserWithMinimumFields() {
        JsonNode receivedWorker;
        String workerStr = readFile("templates/worker.json");
        Worker requestedWorker = Worker.builder().email(faker.internet().emailAddress()).name(faker.gameOfThrones()
                .character()).tasks(new ArrayList<>() {{
            add(12);
            add(13);
        }}).companies(new ArrayList<>() {{
            add(36);
        }}).build();
        try {
            receivedWorker = request.when()
                    .body(requestedWorker)
                    .post()
                    .then()
                    .log().all()
                    .assertThat()
                    .statusCode(201)
                    .body("email", equalTo(requestedWorker.getEmail()))
                    .body("name", equalTo(requestedWorker.getName()))
                    .body("tasks", hasSize(2))
                    .body("tasks[0].id", equalTo(new WorkerResponse.Task(requestedWorker.getTasks().get(0)).getId()))
                    .body("tasks[1].id", equalTo(new WorkerResponse.Task(requestedWorker.getTasks().get(1)).getId()))
                    .body("companies.id", hasItem(requestedWorker.getCompanies().get(0)))
                    .extract()
                    .as(JsonNode.class);
        } finally {
            restUtils.close();
        }
        WorkerResponse convertedWorker = new ObjectMapper().convertValue(receivedWorker, WorkerResponse.class);
    }

    @SneakyThrows
    @Test
    @Step("Регистрация работника без задач")
    void createUserWithoutTasks() {
        String workerStr = readFile("templates/worker.json");
        Worker requestedWorker = createWorker(workerStr);
        requestedWorker.setTasks(null);
        createUserWithMalformedFields(requestedWorker, "Параметр tasks является обязательным!");
    }

    @SneakyThrows
    @Test
    @Step("Регистрация работника без компаний")
    void createUserWithoutCompanies() {
        String workerStr = readFile("templates/worker.json");
        Worker requestedWorker = createWorker(workerStr);
        requestedWorker.setCompanies(null);
        createUserWithMalformedFields(requestedWorker, "Параметр companies является обязательным!");
    }

    @SneakyThrows
    @Test
    @Step("Регистрация работника без емейла")
    void createUserWithoutEmail() {
        String workerStr = readFile("templates/worker.json");
        Worker requestedWorker = createWorker(workerStr);
        requestedWorker.setEmail("");
        createUserWithMalformedFields(requestedWorker, "email неправильный!");
    }

    private Worker createWorker(String workerStr) throws com.fasterxml.jackson.core.JsonProcessingException {
        return new ObjectMapper().readValue(String.format(workerStr, faker.internet().emailAddress(), faker.gameOfThrones().character(), faker.numerify("############")), Worker.class);
    }

    private void createUserWithMalformedFields(Worker requestedWorker, String errorMessage) {
        JsonNode response;
        try {
            response = request.when()
                    .body(requestedWorker)
                    .post()
                    .then()
                    .log().all()
                    .assertThat()
                    .statusCode(400)
                    .extract()
                    .as(JsonNode.class);
        } finally {
            restUtils.close();
        }
        ErrorResponse errorResponse = new ObjectMapper().convertValue(response, ErrorResponse.class);
        Assert.assertEquals(errorResponse, new ErrorResponse("error", errorMessage));
    }


    String readFile(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream(fileName);
        return new BufferedReader(
                new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
    }
}
