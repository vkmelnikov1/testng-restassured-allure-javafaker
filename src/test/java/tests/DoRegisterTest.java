package tests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import models.ErrorResponse;
import models.User;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.BaseTest;
import utils.RestUtils;
import utils.TestInfoContext;

import java.lang.reflect.Method;

public class DoRegisterTest extends BaseTest {

    RequestSpecification request;
    RestUtils restUtils;

    @BeforeMethod(alwaysRun = true)
    void setUp(Method method) {
        TestInfoContext.addTest(method.getAnnotation(Step.class).value());
        request = (restUtils = new RestUtils()).given()
                .log().all()
                .contentType(ContentType.JSON)
                .baseUri("http://users.bugred.ru/tasks/rest/doregister");
    }

    @Test
    @Step("Регистрация пользователя")
    void register() {
        JsonNode receivedUser;
        User requestedUser = new User(faker.internet().emailAddress(), faker.name().firstName(), faker.internet().password());
        try {
            receivedUser = request.when()
                    .body(requestedUser)
                    .post()
                    .then()
                    .log().all()
                    .assertThat()
                    .statusCode(201)
                    .extract()
                    .as(JsonNode.class);
        } finally {
            restUtils.close();
        }
        User convertedUser = new ObjectMapper().convertValue(receivedUser, User.class);
        Assert.assertEquals(convertedUser, requestedUser);
    }

    @Test
    @Step("Регистрация c пустым полем для почты")
    void registerWithEmptyEmail() {
        registerWithMalformedInput(new User("", faker.name().firstName(), faker.internet().password()), "Введите почту");
    }

    @Test
    @Step("Регистрация c коротким паролем")
    void registerWithMalformedPassword() {
        registerWithMalformedInput(new User(faker.internet().emailAddress(), faker.name().firstName(), "12345"), " Пароль должен содержать минимум 5 символов");
    }

    @Test
    @Step("Регистрация пользователя с неправильным емейлом")
    void registerWithMalformedEmail() {
        registerWithMalformedInput(new User("123", faker.name().firstName(), faker.internet().password()), " Некоректный  email 123");
    }

    @Test
    @Step("Регистрация пользователя c пустым паролем")
    void registerWithEmptyPassword() {
        registerWithMalformedInput(new User(faker.internet().emailAddress(), faker.name().firstName(), ""), "Введите пароль");
    }

    @Test
    @Step("Регистрация пользователя c пустым именем")
    void registerWithEmptyName() {
        registerWithMalformedInput(new User(faker.internet().emailAddress(), "", faker.internet().password()), "Введите ваше имя");
    }

    @Test
    @Step("Регистрация пользователя c не уникальной почтой")
    void registerWithDuplicateEmail() {
        registerWithMalformedInput(new User("milli@mail.ru",  faker.name().firstName(), faker.internet().password()), " email milli@mail.ru уже есть в базе");
    }
    private void registerWithMalformedInput(User requestedUser, String errorMessage) {
        JsonNode response;
        try {
            response = request.when()
                    .body(requestedUser)
                    .post()
                    .then()
                    .log().all()
                    .assertThat()
                    .statusCode(400)
                    .extract()
                    .as(JsonNode.class);
        } finally {
            restUtils.close();
        }
        ErrorResponse errorResponse = new ObjectMapper().convertValue(response, ErrorResponse.class);
        Assert.assertEquals(errorResponse, new ErrorResponse("error", errorMessage));
    }

}
