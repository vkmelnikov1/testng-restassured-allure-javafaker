package models;

import lombok.EqualsAndHashCode;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class WorkerResponse {
    private String email;
    private String name;
    private String name1;
    private String hobby;
    private String surname1;
    private String fathername1;
    private String cat;
    private String dog;
    private String parrot;
    private String cavy;
    private String hamster;
    private String squirrel;
    private String phone;
    private String adres;
    private String gender;
    private DateStart date_start;
    private DateUpdated date_updated;
    private Birthday birthday;
    private List<String> role;
    private DateRegister date_register;
    private String date;
    private String by_user;
    private List<Company> companies;
    private List<Task> tasks;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private static class DateStart{
        private long sec;
        private int usec;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private static class DateUpdated{
        private long sec;
        private int usec;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Birthday{
        private long sec;
        private int usec;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class DateRegister{
        private long sec;
        private int usec;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Company{
        private String name;
        private int id;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Task{
        private String name;
        private int id;

        public Task(int id) {
            this.id = id;
        }
    }


}
