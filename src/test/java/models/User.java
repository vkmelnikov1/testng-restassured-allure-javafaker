package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    private String name;
    @EqualsAndHashCode.Exclude
    private String avatar;
    @EqualsAndHashCode.Exclude
    private String password;
    @EqualsAndHashCode.Exclude
    private int birthday;
    private String email;
    @EqualsAndHashCode.Exclude
    private String gender;
    @EqualsAndHashCode.Exclude
    private int date_start;
    @EqualsAndHashCode.Exclude
    private String hobby;

    public User(String email, String name, String password) {
        this.name = name;
        this.password = password;
        this.email = email;
    }


}
