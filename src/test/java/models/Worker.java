package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Worker {
    private String email;
    private String name;
    private List<Integer> tasks;
    private List<Integer> companies;
    private String hobby;
    private String adres;
    private String name1;
    private String surname1;
    private String fathername1;
    private String cat;
    private String dog;
    private String parrot;
    private String cavy;
    private String hamster;
    private String squirrel;
    private String phone;
    private String inn;
    private String gender;
    private String birthday;
    private String date_start;

    public Worker(String email, String name, List<Integer> tasks, List<Integer> companies) {
        this.email = email;
        this.name = name;
        this.tasks = tasks;
        this.companies = companies;
    }

}