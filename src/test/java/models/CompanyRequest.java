package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyRequest {

    private String company_name;
    private String company_type;
    private String[] company_users;
    private String email_owner;

    public CompanyRequest(String company_name, String company_type, String[] company_users, String email_owner) {
        this.company_name = company_name;
        this.company_type = company_type;
        this.company_users = company_users;
        this.email_owner = email_owner;
    }
}
