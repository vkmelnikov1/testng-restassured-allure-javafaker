package models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Company{
    private String name;
    private String type;
    private String inn;
    private String ogrn;
    private String kpp;
    private String phone;
    private String adress;
    private String[] users;
}