package utils;

import static io.restassured.RestAssured.config;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.specification.RequestSpecification;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import lombok.Data;
import lombok.SneakyThrows;

@Data
public class RestUtils {

    private String directory;
    private FileOutputStream outputStream;
    private PrintStream myPrintStream;
    private RequestSpecification specification;
    private RestAssuredConfig config;

    @SneakyThrows
    public RestUtils() {
        Test currentTest = TestInfoContext.getCurrentTest(Thread.currentThread());
        File dir = new File("build/test-logs/");
        if (!dir.exists()) {
            dir.mkdir();
        }
        File dir2 = new File("build/test-logs/" + currentTest.getTestName());
        if (!dir2.exists()) {
            dir2.mkdir();
        }
        File file = new File(
                "build/test-logs/" + currentTest.getTestName() + "/" + currentTest.getTestName() + ".txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        outputStream = new FileOutputStream(file, true);
        myPrintStream = new PrintStream(outputStream);
        config = config().logConfig(new LogConfig(myPrintStream, true));
    }

    public RequestSpecification given() {
        return RestAssured.given()
                .config(config)
                .filter(new AllureRestAssured().setRequestTemplate("custom-http-request.ftl")
                        .setResponseTemplate("custom-http-response.ftl"));
    }

    @SneakyThrows
    public void close() {
        outputStream.close();
        myPrintStream.close();
    }
}