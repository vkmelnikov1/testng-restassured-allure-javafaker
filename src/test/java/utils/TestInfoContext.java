/*
 *
 * =======================================================================
 *
 * Copyright (c) 2009-2020 Sony Network Entertainment International, LLC. All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Sony Network Entertainment International, LLC.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with
 * Sony Network Entertainment International, LLC.
 *
 * =======================================================================
 *
 * For more information, please see http://www.sony.com/SCA/outline/corporation.shtml
 *
 */
package utils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * This class keeps current running tests
 */
public class TestInfoContext {

    public static ConcurrentHashMap<Thread, Test> tests = new ConcurrentHashMap<>();

    public static void addTest(String testName) {
        tests.put(Thread.currentThread(), new Test(testName));
    }

    public static Test getCurrentTest(Thread currentThread) {
        return tests.get(currentThread);
    }
}
