package utils;

import lombok.SneakyThrows;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtils {

    @SneakyThrows
    public static long getTimeInSeconds(String time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime((new SimpleDateFormat("dd.MM.yyyy").parse(time)));
        return calendar.getTimeInMillis() * 1000000000;
    }
}
