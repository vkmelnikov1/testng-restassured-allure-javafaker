package utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.github.javafaker.Faker;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BaseTest {

    protected static Faker faker;

    static {
        Set<String> loggers = new HashSet<>(
                Arrays.asList("org.apache.http", "groovyx.net.http", "io.restassured.internal"));

        for (String log : loggers) {
            Logger logger = (Logger) LoggerFactory.getLogger(log);
            logger.setLevel(Level.INFO);
            logger.setAdditive(false);
        }
        faker = new Faker();
    }

    @SneakyThrows
    @BeforeSuite(alwaysRun = true)
    static void createTestResultsFolder() {
        File testLogsDir = new File("build/test-logs/");
        if (testLogsDir.exists()) {
            FileUtils.deleteDirectory(testLogsDir);
        }
        testLogsDir.mkdir();
    }

}
